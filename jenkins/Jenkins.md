# Jenkins: do zero ao deploy

## O que é:

O Jenkins é uma aplicação de código aberto muito utlizada para automação de tarefas. Ele foi criado em 2004, por Kohsuke Kawaguchi, a partir de um fork do projeto Hudson da Sun. Com o Jenkins podemos controlar todo o clico de vida de uma aplicação, desde a construção, realização de testes, análises estáticas ate a sua implantação.

## Features:

- O Jenkins é uma aplicação feita em java, o que a torna multiplataforma.
- Ele possui centenas de plugins na sua central de aplicação, o que lhe permite ser integrado com inúmeras aplicações.
- Possui inúmeras formas de tarefas: Freestyle project, pipelines, Multibranch Pipeline, etc.
- É possivel rodar o Jenkins com Libvirt, Kubernetes, Docker, Nomad, etc.
- Ele possui um cardápio de apis que nos permite: extrair metadas, atualizar descrição de jobs, deletar-habilidar-desabilitar jobs, etc.

## Vantagens:

- Permite inúmeras customizações
- Controle completo do sistema
- E ...

![free](./free.png)

## Desvantagens:

- Em um primeiro momento, precisamos gastar um pouco tempo para configura-lo.
- Requer um servidor (servidores) dedicados.

## Laboratório:

Para o nosso handson iremos utilizar o docker para provisionar o docker.

```sh
docker run -d --name jenkins -p 8080:8080 -p 50000:50000 jenkins/jenkins:lts
```

Pronto, já temos o nosso jenkins rodando =D.

![easy](./easypeasy.webp)

Depois de alguns segundos, se tentarmos acessar a porta em que o Jenkins esta rodando veremos a seguinte mensagem:

![jenkins-secret](./jenkins-secret.png)

Temos duas formas de acessar essa senha, podemos entrar dentro do container (manualmente ou com o portainer) e dar um **cat** neste arquivo ou podemos simplesmente utilizar o exec para recuperar a chave.

```sh
docker exec -it d2784ead30a4 bash
cat /var/jenkins_home/secrets/initialAdminPassword && exit
## ou apenas
docker exec jenkins  cat /var/jenkins_home/secrets/initialAdminPassword
```

Em seguida deveremos realizar a customização do jenkins. Ele já nos oferece algumas sugestões de plugins ou podemos selecionar o que queremos.

Em seguida, teremos que configurar as informações do usuário admin e informações referentes a instância do jenkins:

![jenkins-admin](./jenkins-secret.png)
![ip](./ip.png)

## Nosso primeiro job

Para criarmos nosso primeiro job devemos clicar em _Novo Job_, selecionar o tipo de job e inserimos o nome do mesmo.
![novo-job](./novo-job.png)

Agora nós preciamos configurar a tarefa que este job executará. Na seção de build, vamos escolher a opção _Executar Shell_ e, em seguida, definiremos a tarefa a ser executada. Para efeitos de demonstração iremos apenas imprimir uma mensagem.

![build-free](./build-free.png)
![resultado-free](./resultado-free.png)

## Pipelines

### O que é um pipeline?

![pipeline-example](./pipeline-example.png)

Um pipeline nada mais é do que uma sequência de passos que devem ser executados para se atingir um objetivo desejado.
A figura acima representa um pipeline de entrega continua. A imagem contém um conjunto de eventos: build, deploy, test e release. Onde cada evento esta interligado com o evento anterior.

## Mãos a obra

Agora iremos criar um novo job em forma de pipeline. Para isto, devemos clicar em _Novo Job_ e selecionar a opção _Pipeline_.

![pipeline-example](./pipeline.png)

Com o job criado, iremos escrever o nosso pipeline. O pipeline deve ser escrito utilizando a linguagem groovy.
Como podemos ver no snippet abaixo, cada etapa do pipeline é definida pela palavra stage. E dentro de cada stage, definimos as atividades a serem executadas.

```groovy
node {

    stage('Clone projeto') {
        git credentialsId: 'gitlab', url: 'https://gitlab.com/claudio.junior/devops-memory.git'
    }

    stage('Configuracao do nodejs') {
        env.NODEJS_HOME = "${tool 'node10'}"
        env.PATH = "${env.NODEJS_HOME}/bin:${env.PATH}"
        echo env.SURGE_TOKEN
    }

    stage('Fetch do gitbook') {
        sh('gitbook fetch  3.2.3');
    }

    stage('Build do livro') {
       sh('gitbook build');
    }

    stage('Deploy do livro') {
        dir('_book') {
            sh('surge --domain book-devops-memory.surge.sh --project ./')
        }
    }
}
```

Neste nosso exemplo, estamos utilizando duas ferramentas: o Gitbook e o surge.sh. Para que o pipeline acima funcione, precisamos configurar o node, em seguida instalar o giitbook e o surge.

# ISSO UMA ALTERAÇÃO PARA VERIFICAR SE O PIPELINE ESTA CORRETO E FAZENDO O DEPLOY UTILIZANDO O SURGE.SH

![pipeline-devops](./pipeline-devops.png)
