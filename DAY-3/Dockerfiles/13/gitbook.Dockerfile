FROM node:10

LABEL AUTHOR="claudio.junior"
LABEL APP="gitbook"

WORKDIR /usr/app/book

RUN npm install gitbook-cli -g