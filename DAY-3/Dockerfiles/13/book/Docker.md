# Docker

O que são containers: 
=======
Um container é um conjunto composto por uma aplicação e suas dependência que compartilham o kernel do sistema operacional da máquina host.

São parecidas com máquinas virtuais, entretanto são mais leves e mais integrados a máquina host, o que permite um melhor desempenho por conta do gerenciamento de recursos.


Diferenças entre VM e CONTAINER
=======
![vmxcontainer](./vm-container.png)

1. Não é necessário emular um novo sistema operacional quando estamos utilizando containers.
1. A virtualização permite o isolamento total do ambiente da sua aplicação, já que ela virtualiza a máquina por completo. O containere compartilha um mesmo kernel do sistema operacional, levando a um isolamento apenas parcial. 
1. Containers necessitam de menos recursos.


## Kernel features

Como dito anteriormente, o docker utiliza recursos do kernel linux para o seu funcionamento. Na imagem abaixo podemos visualizar os módulos utilizados:

![vmxcontainer](./docker-kernel.png)

## Namespaces

Este módulo permite o isolamento de processos. Ou seja, graças a este módulo, cada container possui a sua própria árvore de processos, pontos de montagem, etc, fazendo com que um container não interfira na execução de outro.

## Cgroups

Este módulo é responsável pela limitação do uso de recursos do host pelos containers. É o cgroup que permite o gerenciamento de cpu, memória, I/O, etc.

## Netfilter

Este módulo é utilizado para que os containers consigam se comunicar. Através dele o Docker constrói diversas regras de roteamento.


## Instalação
```bash
 sudo apt-get update
 sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
 echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
 sudo apt-get update
 sudo apt-get install docker-ce docker-ce-cli containerd.io
``` 
## Adhoc

### O que são comandos adhoc?

Comandos utilizados para uma única finalidade.

Estrutura dos comandos adhoc:

- docker container [comando]
- docker image [comando]
- docker volume [comando]
- docker network [comando]


## Lista de comandos: 

```bash
docker container start [CONTAINER ID]
docker container stop [CONTAINER ID]
docker container restart [CONTAINER ID]
docker container pause [CONTAINER ID]
docker container unpause [CONTAINER ID]
docker container inspect [CONTAINER ID]
docker container logs -f [CONTAINER ID]
docker container rm [CONTAINER ID]
docker container attach [CONTAINER ID]
docker container rm -f [CONTAINER ID]
docker container exec -ti [CONTAINER ID] [COMANDO]
docker container run -d nginx
docker container stats [CONTAINER ID]
docker container top [CONTAINER ID]
docker container run -d -m 128M --cpus 0.5 nginx
docker container update --memory 64M --cpus 0.4 nginx
docker container inspect [CONTAINER ID]
docker container stats [CONTAINER ID]
docker container top [CONTAINER ID]
```


# Dockerfile

O [dockerfile](https://docs.docker.com/engine/reference/builder/) é como se fosse a "planta baixa" de um container. Lá é onde será determinado todos os detalhes do seu container, como, por exemplo, a imagem a ser utilizada, aplicativos que necessitam ser instalados, comandos a serem executados, os volumes que serão montados, etc.

### Exemplo dockerfile:
```Dockerfile
FROM alpine

RUN /bin/echo "HELLO DOCKER"
```

### Sintaxe:
```Dockerfile
FROM # Indica qual imagem será utilizada como base, ela precisa ser a primeira linha do Dockerfile;
ADD #Copia novos arquivos, diretórios, arquivos TAR ou arquivos remotos e os adicionam ao filesystem do container;
COPY # Copia novos arquivos e diretórios e os adicionam ao filesystem do container;
ENTRYPOINT # Permite você configurar um container para rodar um executável, e quando esse executável for finalizado, o container também será;
ARG # Permite você configurar argumentos a serem passados para construção da imagem.
CMD # Executa um comando, diferente do RUN que executa o comando no momento em que está "buildando" a imagem, o CMD executa no início da execução do container;
LABEL # Adiciona metadados a imagem como versão, descrição e fabricante;
ENV # Informa variáveis de ambiente ao container;
MAINTAINER # Autor da imagem; 
RUN # Executa qualquer comando em uma nova camada no topo da imagem e "commita" as alterações. Essas alterações você poderá utilizar nas próximas instruções de seu Dockerfile;
USER # Determina qual o usuário será utilizado na imagem. Por default é o root;
VOLUME # Permite a criação de um ponto de montagem no container;
WORKDIR # Responsável por mudar do diretório / (raiz) para o especificado nele;
EXPOSE # Informa qual porta o container estará ouvindo;

```

Quando estamos trabalhando com o ENTRYPOINT e o CMD dentro do mesmo dockerfile, o CMD somente aceita parâmetros do ENTRYPOINT.

## Docker-compose

O [Compose](https://docs.docker.com/compose/) é uma ferramenta para escrever, em um único arquivo, o provisionamento de toda a stack de sua aplicação. Ex: no docker-compose definimos quais os serviços a serem criados e as características de cada service.

O arquivo compose utiliza a linguagem [YAML](https://en.wikipedia.org/wiki/YAML) para a configuração dos services de sua aplicação. Assim, com apenas um único comando, podemos criar e inicilizar todos os containers de nossa pilha de aplicações.


## Instalação
Para instalar o docker-compose, siga os passos abaixo:

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

## testing
docker-compose --version
```

```yml
version: "3.9"  # optional since v1.27.0
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
      - logvolume01:/var/log
    links:
      - redis

  redis:
    image: redis

volumes:
  logvolume01: {}
```