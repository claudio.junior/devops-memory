# Configuração do ambiente

Para realização dos labs será necessário a instalação do VirtualBox e Vagrant.

## Instalação VirtualBox

Para instalar o VirtualBox, basta clicar no [link](https://download.virtualbox.org/virtualbox/6.1.26/VirtualBox-6.1.26-145957-Win.exe), realizar o download do executável e instala-lo.

Distribuições Linux devem seguir os passos descritos [aqui](https://www.virtualbox.org/wiki/Linux_Downloads).

## Instalação Vagrant

Para instalar o Vagrant, basta clicar no link [x86](https://releases.hashicorp.com/vagrant/2.2.18/vagrant_2.2.18_i686.msi) ou [x64](https://releases.hashicorp.com/vagrant/2.2.18/vagrant_2.2.18_x86_64.msi), realizar o download do executável e instala-lo.

## Instalação Ansible

Para instalar o Ansible execute os comandos abaixo:

```sh
sudo apt-add-repository ppa:ansible/ansible -y
sudo apt-get update && sudo apt-get install ansible -y
```

## Instalação Docker

Para instalar o Docker execute o comando abaixo:


```sh
curl -fsSL https://get.docker.com | sh

```