FROM golang AS build

ENV version 1.3.0

RUN curl -LO https://github.com/prometheus/node_exporter/releases/download/v${version}/node_exporter-${version}.linux-amd64.tar.gz \
    && tar -xvzf node_exporter-${version}.linux-amd64.tar.gz \
    && cp node_exporter-${version}.linux-amd64/node_exporter /tmp/


FROM alpine

COPY --from=build /tmp/node_exporter /usr/local/bin
EXPOSE 9100
ENTRYPOINT node_exporter --path.procfs=/usr/proc --path.sysfs=/usr/sys \
    --collector.filesystem.ignored-mount-points="^(/rootfs|/host|)/(sys|proc|dev|host|etc)($$|/)" \ 
    --collector.filesystem.ignored-fs-types="^(sys|proc|auto|cgroup|devpts|ns|au|fuse\.lxc|mqueue)(fs|)$$"
