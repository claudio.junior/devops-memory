FROM alpine:latest

ENV version=0.23.0

RUN wget https://github.com/prometheus/alertmanager/releases/download/v${version}/alertmanager-${version}.linux-amd64.tar.gz && \
    tar -xvzf alertmanager-${version}.linux-amd64.tar.gz  && \
    rm -rf alertmanager-${version}.linux-amd64.tar.gz && \
    cp alertmanager-${version}.linux-amd64/alertmanager /bin && \
    rm -rf alertmanager-${version}.linux-amd64 


ENTRYPOINT ["alertmanager"]
CMD ["--config.file=/etc/alertmanager/config.yml", "--storage.path=/alertmanager"]