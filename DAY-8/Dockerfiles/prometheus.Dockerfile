FROM alpine

ENV version=2.31.1

RUN adduser -s /bin/false -D -H prometheus && \
    adduser -s /bin/false -D -H node_exporter && \
    wget https://github.com/prometheus/prometheus/releases/download/v${version}/prometheus-${version}.linux-amd64.tar.gz && \
    tar -xvzf prometheus-${version}.linux-amd64.tar.gz && \
    mkdir -p /etc/prometheus /var/lib/prometheus && \
    cp prometheus-${version}.linux-amd64/promtool /usr/local/bin/ && \
    cp prometheus-${version}.linux-amd64/prometheus /usr/local/bin/ && \
    cp -R prometheus-${version}.linux-amd64/console_libraries/ /etc/prometheus/ && \
    cp -R prometheus-${version}.linux-amd64/consoles/ /etc/prometheus/ && \
    chown prometheus:prometheus /usr/local/bin/prometheus && \
    chown prometheus:prometheus /usr/local/bin/promtool && \
    chown -R  prometheus:prometheus /etc/prometheus/ && \
    chown prometheus:prometheus /var/lib/prometheus && \
    rm -rf prometheus-${version}.linux-amd64/

USER prometheus

ENTRYPOINT /usr/local/bin/prometheus \
    --config.file /etc/prometheus/prometheus.yml \
    --storage.tsdb.path /var/lib/prometheus \
    --web.console.libraries=/usr/share/prometheus/console_libraries \
    --web.console.templates=/usr/share/prometheus/consoles