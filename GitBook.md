GitBook
=======

GitBook é uma ferramenta de linha de comando (e biblioteca Node.js) para construir belos livros usando GitHub / Git e Markdown (ou AsciiDoc). 

Você pode publicar e hospedar livros online facilmente usando [gitbook.com](https://legacy.gitbook.com). Um editor de desktop [também disponível](https://legacy.gitbook.com/editor).

Fique atualizado seguindo [@GitBookIO](https://twitter.com/GitBookIO) no Twitter ou [GitBook](https://www.facebook.com/gitbookcom) no Facebook.

A documentação completa está disponível em [toolchain.gitbook.com](http://toolchain.gitbook.com/).

## Construindo localmente

Para trabalhar localmente com este projeto, você terá que seguir as etapas abaixo:

1. Fork, clone ou baixe este projeto
1. Instale GitBook: npm install gitbook-cli -g
1. Visualize seu projeto: gitbook serve
1. Adicionar conteúdo
1. Gere o site: gitbook build (opcional)
1. Envie suas alterações para o branch master: git push

Leia mais na [documentação](https://docs.gitbook.com/v2-changes) do GitBook .