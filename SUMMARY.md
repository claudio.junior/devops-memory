# Summary

- [Introdução](README.md)
- [Vagrant](Vagrant.md)
- [Ansible](Ansible.md)
- [Docker](Docker.md)
- [Monitor](Monitor.md)
- [Loki](Loki.md)
- [Jenkins](./jenkins/Jenkins.md)
- [Gitlab-ci](./gitlabci/gitlabci.md)
- [GitBook](GitBook.md)
- [Update de um node](UpdateDocker.md)
- [SemanticVersion](./cz/Cz.md)
