# Gitlab-CI

## O que é?

O GitLab Ci é um ambiente de Integração Contínua, que pertence ao GitLab. Através dela podemos implementar toda a nossa stack de continuous integration, continuous deployment e continuous delivery de forma simples. Para utilizarmos este recurso devemos apenas escrever o arquivo _.gitlab-ci.yml_.

# [Jobs:](https://docs.gitlab.com/ee/ci/jobs/index.html)

Toda configuração de um pipeline começa com um Job, ele é o elemento mais fundamental de um arquivo gitlab-ci.yml.
Com o gitlab ci, nós somos podemos ter inúmeros jbos além de sermos capazes de definir as condições sob os quais um job deve ser executado.

```yml
job1:
  script: "uname -a"

job2:
  script:
    - pwd
    - whoami
    - npm --version #?
```

# [Scripts](https://docs.gitlab.com/ee/ci/yaml/index.html#script):

Scripts são utilizados para definir comandos a serem executados por um runner.
Todos os jobs necessitam ter um script definido, exceto pelos trigger jobs.

```yml
job1:
  script: "uname -a"

job2:
  script:
    - pwd
    - whoami
    - npm --version #?
```

## Before/After script

Utilizados no job para executarem comandos antes de depois de um determinado script.

Obs: after_script executa independentemente se o script falhar.

```yml
job1:
  before_script:
    - whoami
  script: "uname -a"
  after_script:
    - whoami
```

# [Image:](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html)

A plavara chave _Image_ é o nome de uma imagem docker que um determinado executará rodará um determinado job.
Por default, os runners baixará as imagems do Dockerhub, mas é possivel configurár para baixar as imagens de um registry particular.

```yml
image: node:latest
job1:
  script: "uname -a"

job2:
  script:
    - pwd
    - whoami
    - npm --version #?
```

# [Stages:](https://docs.gitlab.com/ee/ci/yaml/index.html#stages)

Stages são utilizados para agrupar um conjunto de jobs. Se você não definir nenhum stage, o gitlab assumirá os seguintes estágios como default:

- .pre: Utilizado para rodar um job no inicio do pipeline.
- build
- test
- deploy
- .post: Utilizado para rodar um job no final do pipeline.

Importante dizer que um stage roda todos os jobs dentro dele em paralelo.

```yml
image: node:latest

stages:
  - build
  - test
  - deploy

job1:
  stage: build
  script: "uname -a"

job2:
  stage: test
  script:
    - pwd
    - whoami
    - npm --version #?
```

# [Artifacts:](https://docs.gitlab.com/ee/ci/yaml/index.html#artifacts)

Utilizamos o artifact para salvar o resultado de um job. Podemos definir uma lista de arquivos e diretorios para serem salvos.
O artefato gerado é enviado para o Gitlab depois do job ser finalizado e eles ficam disponiveis na UI do gitlab.

É importante dizer que os artefatos gerados nos primeiros estágios ficam disponíveis nos estágios posteriores.

```yml
image: node:10
stages:
  - build
  - test
  - deploy

buildando site:
  stage: build
  before_script:
    - npm install gitbook-cli -g
    - gitbook fetch 3.2.3
    - gitbook install
  script:
    - gitbook build
  artifacts:
    name: nome-de-exemplo
    paths:
      - _book
    expire_in: 1 hour
```

# [Cache:](https://docs.gitlab.com/ee/ci/yaml/index.html#cache)

O cache é semelhante ao artifacts. Utilizado para trafegar arquivos entre jobs e/ou pipelines.
O cache é restaurado antes dos artifacts.

```yml
image: node:10
stages:
  - build
  - test
  - deploy

cache:
  key: book
  paths:
    - _book

buildando site:
  stage: build
  before_script:
    - npm install gitbook-cli -g
    - gitbook fetch 3.2.3
    - gitbook install
  script:
    - gitbook build

deploy surge:
  stage: deploy
  cache:
    key: book
    paths:
      - _book
    policy: pull
  before_script:
    - npm install surge -g
  script:
    - surge --domain book-devops-memory.surge.sh --project ./_book
  only:
    - main
```

# [Environment](https://docs.gitlab.com/ee/ci/yaml/index.html#environment)
