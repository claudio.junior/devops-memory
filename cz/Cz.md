# Padronização de merges

## O que é ```Semantic Versioning```?

É uma especificação utilizada para determinar qual será o número da versão atribuida e incrementada ao seu projeto. Segundo ela, todo número de versão deve estar em conformidade com a estrutura abaixo:

![version](./version.png)

Onde,

* MAJOR(BreakingChanges): Indica alguma alteração importante.
* MINOR(Features): Indica um recurso ou funcionalidade.
* PATCH(Bugs): Indica correção de bugs.

## Por que usar Versionamento Semântico?

O uso desse framework não é uma novidade, inúmeros projetos consolidados já o utilizam, como por exemplo: angular, jenkins, terraform, visualcode, etc. Este padrão nos fornece uma especificação formal para o gerenciamento de dependências. Atraves dela, somos capazes de comunicar nossas intenções aos usuários do nosso software. Se não tivermos um padrão para os números de versões, eles serão inúteis para o gerenciamento de dependências.

## O que é ```Conventional Commits```:

Conventional Commit é uma especifição que define um conjunto de regras a serem seguidas quando escrevemos uma mensagem de um commit. Ele segue as diretrizes de mensagens do Angular e possui a estrutura abaixo.

```sh
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```
Os únicos campos obrigatórios são ```type``` e ```subject``` todo o resto é opcional. 

Existem vários valores que o ```type``` pode assumir, logo abaixo temos a lista dos tipos e uma breve descrição do seu significado.

```sh
feat: A new feature
fix: A bug fix
docs: Documentation only changes
style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
refactor: A code change that neither fixes a bug nor adds a feature
perf: A code change that improves performance
test: Adding missing or correcting existing tests
chore: Changes to the build process or auxiliary tools and libraries such as documentation generation
```
Exemplo de commite:

```sh
feat(ESO-1034): criacao do atualizador linux para o esocial.

Nesta branch encontra-se implementado o endpoint que retornará a versão do cliente esocial. 
Além disto, foi implementado o endpoint que realizará o agendamento para o download da versão mais recente da aplicação.
```
## Commitzen (cz-cli):

Para facilitar a padronização de commits, podemos utilizar a ferramenta de cli commitezen.
A instalação é simples, precisamos apenas ter o node instalado em nossa máquina. Para instalar, basta executar o comando npm abaixo:

```
npm install -g commitizen cz-conventional-changelog && echo '{ "path": "cz-conventional-changelog" }' > ~/.czrc
```

Após a execução o resultado deve ser semelhante a este:

```bash
➜ devops-memory (master) ✗ npm install -g commitizen cz-conventional-changelog && echo '{ "path": "cz-conventional-changelog" }' > ~/.czrc

/home/claudio/.nvm/versions/node/v14.15.0/bin/cz -> /home/claudio/.nvm/versions/node/v14.15.0/lib/node_modules/commitizen/bin/git-cz
/home/claudio/.nvm/versions/node/v14.15.0/bin/commitizen -> /home/claudio/.nvm/versions/node/v14.15.0/lib/node_modules/commitizen/bin/commitizen
/home/claudio/.nvm/versions/node/v14.15.0/bin/git-cz -> /home/claudio/.nvm/versions/node/v14.15.0/lib/node_modules/commitizen/bin/git-cz
+ cz-conventional-changelog@3.3.0
+ commitizen@4.2.4
added 70 packages from 124 contributors and updated 1 package in 5.477s
```

Após a instalação, já seremos capazes de utiliza-lo. Para isto, basta utilizar o comando `git cz` ou apenas `cz` em vez de `git commit`.

![md](./install.png)

## Commits Memory

Por fim, para facilitar a geração de changelogs, chegamos a um consenso. Todos os commits devem seguir o padrão ```Conventional Commits```. Outro ponto que deve ser mencionado é que na tag ```<scope>```, devemos colocar o Id do card do Jira associado com a implementação.