# Grafana Loki

## O que é:

O Grafana Loki é um sistema de agregação de log multi-tenant, inspirado no prometheus. É um projeto onpensource iniciado pela Grafana em 2018 e lançado sob a licença Apache 2.0. Ele é usado principalmente para ambientes de núvens (pública ou privada) e com ferramentas como Prometheus e Grafana.

O Loki é semelhante a stack ELK / EFK, mas é bem mais fácil de configurar e operar. O Loki não indexa o conteúdo do log, em vez disso ele indexa timestamps e labels para um stream de log. Isso torna o índice menor, o que simplifica as operações e, eventualmente, reduz o custo.

## Vantagens:

1. O Loki é bem economico pois idexa apenas metadados.
1. Oferece suporte a multi-tenant.
1. Podemos utilizar o Loki localmente para sistemas de pequena escala, ou podemos escala-lo horizontalmente para operações de larga escala.
1. Fácil de conectar com ferramentas populares como Swarm, Kubernetes, Prometheus e visualização no Grafana.

## Stack:

![stack-loki](./stack-loki.png)

## Instalando Loki e Promtail

1. Acesse a página de [releases](https://github.com/grafana/loki/releases/) e faça o download os executáveis do Loki e Promtail.

```sh
wget https://github.com/grafana/loki/releases/download/v2.4.1/loki-linux-amd64.zip
wget https://github.com/grafana/loki/releases/download/v2.4.1/promtail-linux-amd64.zip
```

2. Realize a descompactação dos arquivos.

```sh
sudo apt-get install unzip
unzip loki-linux-amd64.zip
unzip promtail-linux-amd64.zip
```

3. Faça o download dos arquivos de configuração.

```sh
wget https://raw.githubusercontent.com/grafana/loki/master/cmd/loki/loki-local-config.yaml
wget https://raw.githubusercontent.com/grafana/loki/main/clients/cmd/promtail/promtail-local-config.yaml
```

4. Altere a permissão dos arquivos e mova-os para /usr/local/bin

```sh
mv promtail-linux-amd64 /usr/local/bin/
mv promtail-local-config.yaml /usr/local/bin/
chmod u+x /usr/local/bin/promtail-linux-amd64

mv loki-linux-amd64 /usr/local/bin/
mv loki-local-config.yaml /usr/local/bin/
chmod u+x /usr/local/bin/loki-linux-amd64
```

5. Execute o loki e o promtail

```sh
loki-linux-amd64 -config.file=/usr/local/bin/loki-local-config.yaml
promtail-linux-amd64 -config.file=/usr/local/bin/promtail-local-config.yaml
```

## Rodando como serviço

1. Primeiro, crie um usuário para cada aplicação

```sh
sudo useradd --system promtail
sudo useradd --system loki

usermod -aG adm promtail
usermod -aG adm loki
```

2. Em seguida, crie o serviço para o promtail e para o loki.

```sh
# Promtail service
cat << EOF > /etc/systemd/system/promtail.service
[Unit]
Description=Promtail service
After=network.target

[Service]
Type=simple
User=promtail
ExecStart=/usr/local/bin/promtail-linux-amd64 -config.file /usr/local/bin/promtail-local-config.yaml

[Install]
WantedBy=multi-user.target
EOF

# Loki service
cat << EOF > /etc/systemd/system/loki.service
[Unit]
Description=Loki service
After=network.target

[Service]
Type=simple
User=loki
ExecStart=/usr/local/bin/loki-linux-amd64 -config.file /usr/local/bin/loki-local-config.yaml

[Install]
WantedBy=multi-user.target
EOF
```

3. Por fim, teste os serviços criados.

```sh
sudo service promtail start
sudo service promtail stop
sudo service promtail status
sudo systemctl enable promtail.service


sudo service loki start
sudo service loki stop
sudo service loki status
sudo systemctl enable loki.service
```

## Docker

Para termos o Grafana Loki em um container precisamos apenas de um docker-compose.yml, pois já existem as imagens do Promtail e do Loki no dockerhub.

```yaml
version: "3.8"
services:
  grafana:
    image: claudioharu/grafana:latest
    ports:
      - 3000:3000
    env_file:
      - grafana.config
    user: root

  loki:
    image: grafana/loki:2.0.0
    ports:
      - "3100:3100"
    volumes:
      - /var/log:/var/log
    command:
      - -config.file=/etc/loki/local-config.yaml
      - -print-config-stderr=true

  promtail:
    image: grafana/promtail:2.0.0
    command: -config.file=/etc/promtail/config.yml
    volumes:
      - type: bind
        source: ./promtail/config.yml
        target: /etc/promtail/config.yml

      - type: bind
        source: /var/log
        target: /host-logs

    depends_on:
      - loki
    ports:
      - 9080:9080
```

## Coletar logs dos containers

Para realizarmos a coleta dos logs dos containers precisamos instalar o plugin do Loki no docker.

```sh
docker plugin install grafana/loki-docker-driver:latest --alias loki --grant-all-permissions
```

Além disto, precisamos alterar o daemon.json para definirmos o default loggin driver.
Para isto, vá em /etc/docker e altere o arquivo daemon.json, caso ele não exista basta cria-lo.

```json
{
  "debug": true,
  "log-driver": "loki",
  "log-opts": {
    "loki-url": "http://localhost:3100/loki/api/v1/push",
    "loki-batch-size": "400"
  }
}
```

Ao finalizar precisamos reiniciar o serviço do docker.

Além disto precisamos incluir um job para a cotela de logs dos containers no arquivo de configuração do promtail.

```yaml
- job_name: docker
  pipeline_stages:
    - docker: {}
  static_configs:
    - labels:
        job: docker
        __path__: /var/lib/docker/containers/*/*-json.log
```

## Fácil né?!

![Loki](./loki.gif)
