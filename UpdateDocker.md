# Atualizando o docker de um node :

1. Executar o comando docker "docker node ls" para listar os nodes do cluster.
2. Verificar qual dos nodes está com a versão do docker indesejada.
3. Executar o comando "docker node inspect NAME ",para encontrar o ip da máquina em que este node está rodando.
4. Caso o node em que será feita a manutenção for um leader ou estiver com o status reachable , é importante utilizar o comando  "docker node demote NAME"  
5. Executar o comando "docker node update --availability drain NAME". Desta forma ele se mostra em manutenção para o cluster.
6. Executar um ssh para o ip da maquina desejada,e executar os comandos de update e upgrade de acordo com a distro de sua máquina.Ex: ubuntu: sudo apt-get update && sudo apt upgrade  Ex2: centos: sudo yum update -y 
7. Após o fim da execução dos updates,verificar se a versão do docker foi atualizada com o comando "docker --version"
8. Caso tudo estiver correto,dar o comando "exit" para sair da conexão ssh,e executar o comando "docker node update --availability active NAME"
9. Caso tenha feito o demote de um node , executar o comando de promote para o mesmo "docker node promote NAME"
